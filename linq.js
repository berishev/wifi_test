"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var collection = require("./collection");
var LINQ = (function () {
    function LINQ(items) {
        this._items = [];
        if (items == null)
            throw new Error("LINQException: LINQ items == null");
        this._items = items;
    }
    LINQ.prototype.toArray = function () {
        return this._items;
    };
    LINQ.fromArray = function (items) {
        return new LINQ(items);
    };
    LINQ.prototype.copy = function () {
        return this._items.slice();
    };
    LINQ.prototype.where = function (func) {
        var _this = this;
        return LINQ.fromArray(this._items.filter(function (m, i) { return func(m, i, _this) === true; }));
    };
    LINQ.prototype.take = function (count) {
        var items = [].concat(this._items);
        items.splice(count);
        return LINQ.fromArray(items);
    };
    LINQ.prototype.skip = function (count) {
        return LINQ.fromArray([].concat(this._items).splice(count));
    };
    LINQ.prototype.count = function (func) {
        if (func) {
            return this.where(func).count();
        }
        return this._items.length;
    };
    LINQ.prototype.elementAt = function (id) {
        var el = this.elementAtOrNull(id);
        if (el == null)
            throw new Error("LINQException: " + id + " id not found");
        return el;
    };
    LINQ.prototype.elementAtOrNull = function (id) {
        return this._items[id];
    };
    LINQ.prototype.single = function (func) {
        var el = this.singleOrNull(func);
        if (el == null)
            throw new Error("LINQException: single = null");
        return el;
    };
    LINQ.prototype.singleOrNull = function (func) {
        if (func)
            return this.where(func).singleOrNull();
        var count = this.count();
        if (count > 1 || count <= 0)
            return null;
        return this.elementAtOrNull(0);
    };
    LINQ.prototype.first = function (func) {
        var el = this.firstOrNull(func);
        if (el == null)
            throw new Error("LINQException: first = null");
        return el;
    };
    LINQ.prototype.firstOrNull = function (func) {
        if (func)
            return this.where(func).firstOrNull();
        return this.elementAtOrNull(0);
    };
    LINQ.prototype.last = function (func) {
        var el = this.lastOrNull(func);
        if (el == null)
            throw new Error("LINQException: last = null");
        return el;
    };
    LINQ.prototype.lastOrNull = function (func) {
        if (func)
            return this.where(func).lastOrNull();
        return this.elementAtOrNull(this._items.length - 1);
    };
    LINQ.prototype.notNull = function () {
        return this.where(function (m) { return m != null; });
    };
    LINQ.prototype.notEmpty = function () {
        return this.where(function (m) { return !!m; });
    };
    LINQ.prototype.distinct = function (func) {
        var _this = this;
        if (func) {
            return this.select(func).distinct().select(function (m) { return _this.firstOrNull(function (k, i, l) { return func(k, i, l) === m; }); });
        }
        else
            return LINQ.fromArray(this._items.filter(function (value, index, self) { return self.indexOf(value) === index; }));
    };
    LINQ.prototype.except = function (array) {
        var items = array instanceof LINQ ? array._items : array instanceof Array ? array : [array];
        var temp = LINQ.fromArray(items).distinct();
        return LINQ.fromArray(this._items.filter(function (v, i, s) { return temp._items.indexOf(v) === -1; }));
    };
    LINQ.prototype.max = function (func) {
        return Math.max.apply(Math, this.select(func)._items);
    };
    LINQ.prototype.min = function (func) {
        return Math.min.apply(Math, this.select(func)._items);
    };
    LINQ.prototype.reverse = function () {
        return LINQ.fromArray(this._items.reverse());
    };
    LINQ.prototype.average = function (sumFunction, countFunction) {
        return this.sum(sumFunction) / (countFunction ? this.sum(countFunction) : this.count());
    };
    LINQ.prototype.orderBy = function (func) {
        var orderFunc = func ? function (a, b) {
            var x = func(a);
            var y = func(b);
            if (x < y) {
                return -1;
            }
            if (x > y) {
                return 1;
            }
            return 0;
        } : undefined;
        return LINQ.fromArray(this._items.sort(orderFunc));
    };
    LINQ.prototype.forEach = function (func) {
        var _this = this;
        this._items.forEach(function (v, i, s) {
            return func({ item: v, index: i, linq: _this });
        });
        return this;
    };
    LINQ.prototype.orderByDescending = function (func) {
        var orderFunc = func ? function (a, b) {
            var x = func(a);
            var y = func(b);
            if (x > y) {
                return -1;
            }
            if (x < y) {
                return 1;
            }
            return 0;
        } : undefined;
        return LINQ.fromArray(this._items.sort(orderFunc));
    };
    LINQ.prototype.sum = function (func) {
        var sum = 0;
        var index = 0;
        for (var _i = 0, _a = this._items; _i < _a.length; _i++) {
            var item = _a[_i];
            if (!(func || item instanceof Number))
                throw new Error("LINQException: not defined Function or item isn't number");
            sum += func ? func(item, index, this) : Number(item);
            index++;
        }
        return sum;
    };
    LINQ.prototype.ofType = function (type) {
        return this.where(function (item) { return item instanceof type; });
    };
    LINQ.prototype.select = function (func) {
        var _this = this;
        return LINQ.fromArray(this._items.map(function (item, i) { return func(item, i, _this); }));
    };
    LINQ.prototype.selectMany = function (func) {
        return LINQ.fromArray([].concat.apply([], this.select(func).toArray())).distinct();
    };
    LINQ.prototype.groupBy = function (func) {
        var _this = this;
        var out = new collection.Dictionary();
        this.forEach(function (m) {
            var key = func(m.item, m.index, _this);
            if (!out.containsKey(key))
                out.add(key, []);
            var a = out.get(key);
            a.push(m.item);
        });
        return out;
    };
    LINQ.prototype.contains = function (value) {
        return this._items.indexOf(value) !== -1;
    };
    LINQ.prototype.containsAll = function (array) {
        var _this = this;
        return LINQ.fromArray(array).all(function (item) { return _this.contains(item); });
    };
    LINQ.prototype.any = function (func) {
        for (var id = 0; id < this.count(); id++) {
            if (func(this.elementAtOrNull(id)) === true) {
                return true;
            }
        }
        return false;
    };
    LINQ.prototype.all = function (func) {
        for (var id = 0; id < this.count(); id++) {
            if (func(this.elementAtOrNull(id)) === false)
                return false;
        }
        return true;
    };
    LINQ.prototype.concat = function (array) {
        var items = array instanceof LINQ ? array._items : array instanceof Array ? array : [array];
        return LINQ.fromArray(this._items.concat(items));
    };
    LINQ.prototype.intersect = function (array, func) {
        if (func) {
            var arr = array instanceof LINQ ? array._items : array;
            var out = [];
            for (var idx = 0; idx < this.count(); idx++) {
                var item = this.elementAtOrNull(idx);
                for (var idx2 = 0; idx2 < arr.length; idx2++) {
                    var item2 = arr[idx2];
                    if (func(item, item2, idx, idx2) === true) {
                        out.push(item);
                    }
                }
            }
            return LINQ.fromArray(out);
        }
        else {
            return this.except(this.except(array));
        }
    };
    return LINQ;
}());
exports.LINQ = LINQ;
//# sourceMappingURL=linq.js.map